export class UserSession {
    idUtilisateur: number;
    login: string;
    firstName: string;
    lastName: string;
    codeProfil: string;
    token: string;    
  }