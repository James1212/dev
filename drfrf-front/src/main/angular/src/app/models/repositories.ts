export class Repositories {

    private url: String;
    private username: String;
    private password: String;
    private directoryName: String;



    constructor(url: String, username: String, password: String, directoryName: String) {

        this.url = url;
        this.username = username;
        this.password = password;
        this.directoryName = directoryName;

    }

}