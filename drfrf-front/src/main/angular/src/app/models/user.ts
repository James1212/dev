import { Role } from "./role";

export class User{
    id : number;
    firstName : String;
    lastName : String;
    email:String;
    password : String;
    active : Boolean;
    photo : Blob;
    roles : Role[];

    constructor(){}
}