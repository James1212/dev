import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';
import { LoginComponent } from './views/users/login/login.component';
import { GenerateComponent } from './views/generator/generate.component';


export const routes: Routes = [
  {
    path: 'generator',
    component: GenerateComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: '',
    redirectTo: 'dashboard',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      }
    ]
  },
  {
    path: 'users',
    component: FullLayoutComponent,
    data: {
      title: 'Users'
    },
    children: [
      {
        path: '',
        loadChildren: './views/users/users.module#UsersModule',
      }
    ]
  },


];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
