import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { GeneratorService } from "../../services/GeneratorService";
import { SelectItem } from "primeng/api";
import { Dependency } from "../../models/dependency";
import { ValidateUrl } from './validators/url.validator';
import swal from 'sweetalert2';
import { Repositories } from "../../models/repositories";
import { Response } from "@angular/http";
@Component({
    selector: 'generator',
    templateUrl: './generate.component.html',
    styleUrls: ['./generate.component.css'],
    providers: [GeneratorService]
})

export class GenerateComponent implements OnInit {
    code: String = null;
    repository: Repositories = null;
    filterDependenciesMultiple: Dependency[];
    existingDependencies: Dependency[];
    rForm: FormGroup;
    command: String = "mvn archetype:generate -B -DarchetypeArtifactId=webops-archetype -DarchetypeGroupId=com.bycnit.webops";

    constructor(private fb: FormBuilder, private generatorService: GeneratorService) {

        this.rForm = fb.group({
            'groupId': ["com.example", Validators.required],
            'artifactId': ["demo", Validators.required],
            'packageName': ["com.example.demo", Validators.required],
            'repo': ['', [Validators.required, ValidateUrl]],
            'username': ['', Validators.required],
            'password': ['', Validators.required],
            'dependencies': ['', Validators.compose([Validators.required, Validators.minLength(1)])],
        })
    }

    ngOnInit() {

        this.existingDependencies = [];

        //Initilize dependencies 
        this.existingDependencies = [
            { name: 'web', description: 'full-stack web development with tomcat and spring MVC' },
            { name: 'Devtools', description: 'Spring boot development tools' },
            { name: 'Actuator', description: 'Production ready featuresto help you monitor and manage your app' },
            { name: 'JPA', description: 'Java Persistence API including spring-data-jpa and Hibernate' },
            { name: 'Lombok', description: 'Java annotation library which helps to reduce boilerplate code and code faster' }];

    }

    onChangeGroup(groupIdValue: String) {
        if (this.rForm.controls['artifactId'].value === "") {
            this.rForm.controls['packageName'].setValue(groupIdValue + ".demo");
        } else {
            this.rForm.controls['packageName'].setValue(groupIdValue + "." + this.rForm.controls['artifactId'].value);
        }
    }

    onChangeArtifact(artifactIdValue: String) {
        if (this.rForm.controls['groupId'].value === "") {
            this.rForm.controls['packageName'].setValue("com.example." + artifactIdValue + ".demo");
        } else {
            this.rForm.controls['packageName'].setValue(this.rForm.controls['groupId'].value + "." + this.rForm.controls['artifactId'].value);
        }
    }

    //determine the roles which are corresponding to the role filer
    filterDependencyMultiple(event) {
        let query = event.query;
        this.filterDependenciesMultiple = this.filterDependency(query, this.existingDependencies);
    }

    filterDependency(query, dependencies: Dependency[]): Dependency[] {
        //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
        let filtered: Dependency[] = [];
        for (let i = 0; i < dependencies.length; i++) {
            let dependency = dependencies[i];
            if (dependency.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(dependency);
            }
        }
        return filtered;
    }


    //prepare the query and submit generation of the project
    onSubmit(projectDetails, form: NgForm) {


        if (projectDetails.groupId != null) {
            this.command += " -DgroupId=" + projectDetails.groupId;
        }
        if (projectDetails.artifactId != null) {
            this.command += " -DartifactId=" + projectDetails.artifactId;
        }
        if (projectDetails.packageName != null) {
            this.command += " -DpackageName=" + projectDetails.packageName + " -Dversion=1.0-SNAPSHOT";
        }

        this.generatorService.generate(this.command)
            .then(res => {
                
                this.repository = new Repositories(projectDetails.repo, projectDetails.username, projectDetails.password, projectDetails.artifactId);
                this.generatorService.pushSourceCode(this.repository)
                    .then(res => {
                        swal(
                            'Good job!',
                            'You clicked the button!',
                            'success'
                        ).then(res => {
                            form.reset();
                        })
                    })
                    .catch(this.showAlert)
            })
            .catch(this.showAlert);
    }

    showAlert() {
        swal({
            type: 'error',
            title: 'Oops...',
            text: 'Something went wrong!',
            footer: '<a href>Why do I have this issue?</a>',
        })
    }

}