import { AbstractControl } from '@angular/forms';

export function ValidateUrl(control: AbstractControl) {
  if (control.value != null && !control.value.startsWith('http')) {
    return { validUrl: true };
  }
  return null;
}