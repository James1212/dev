import { NgModule } from '@angular/core';


import { RegisterFormComponent } from './register/register-form.component';
import { UserListComponent } from './details/user-list.component';
import { UsersRoutingModule } from './users-routing.module';
import { InputTextModule } from 'primeng/inputtext';
import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button';
import {AutoCompleteModule} from 'primeng/autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FileUploadModule } from 'primeng/fileupload';
import { TableModule } from 'primeng/table';

import { DataTableModule } from 'primeng/datatable';
import { DialogModule } from 'primeng/dialog';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ConfirmationService } from 'primeng/api';
import { GrowlModule } from 'primeng/growl';
import { MultiSelectModule } from 'primeng/multiselect';
import { ListboxModule } from 'primeng/listbox';
import { SliderModule} from 'primeng/slider';
import { ProgressSpinnerModule} from 'primeng/progressspinner';
import { LoginSSOComponent } from './login-sso/loginSSO.component';






@NgModule({
    imports: [
        UsersRoutingModule,
        InputTextModule,
        PanelModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        ButtonModule,
        AutoCompleteModule,
        FileUploadModule,
        TableModule,
        DataTableModule,
        DialogModule,
        ConfirmDialogModule,
        GrowlModule,
        MultiSelectModule,
        ListboxModule,
        SliderModule,
        ProgressSpinnerModule
    ],
    declarations:
        [RegisterFormComponent,
            UserListComponent,
           
            LoginSSOComponent
            
        ]
})
export class UsersModule { }
