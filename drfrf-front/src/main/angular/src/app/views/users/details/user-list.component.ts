import { Component, OnInit } from "@angular/core";
import swal from 'sweetalert2';
import { User } from "../../../models/user";
import { UserListService } from "../../../services/user-list.service";
import { Role } from "../../../models/role";
import { LazyLoadEvent } from "primeng/api";

@Component({
    templateUrl: './user-list.component.html',
    providers: [UserListService]
})
export class UserListComponent implements OnInit {

    dysplayButton: Boolean = true;
    selectedUser: User = new User();
    userList: User[];
    yearFilter: number = 2016;
    yearTimeout: any;
    cols: any[];
    page: number = 0;
    size: number = 10;
    totalPages: number;
    totalElements: number;
    sortDir: string = "DESC";
    sort: string = "firstName";
    loading: boolean;

    //the constructor of the component
    constructor(private userService: UserListService) { }

    //Retrieve datas and initilize the user's list
    ngOnInit() {
        this.getAllUsers(this.page, this.size, this.sort, this.sortDir);
        this.cols = [
            { field: 'firstName', header: 'FIRSTNAME' },
            { field: 'lastName', header: 'LASTNAME' },
            { field: 'email', header: 'EMAIL' },
            { field: 'createDate', header: 'CREATION DATE' },
            { field: 'active', header: 'STATUS' },
        ];
    }

    loadusers(event: LazyLoadEvent) {
        this.loading = true;
        this.sortDir = event.sortOrder > 0 ? 'ASC' : 'DESC';
        this.page = event.first / this.size;
        if (event.sortField != undefined) {
            this.sort = event.sortField;
        }
        setTimeout(() => {
            this.getAllUsers(this.page, this.size, this.sort, this.sortDir);
            this.loading = false;
        }, 1000);
    }
    //delete the selected user from the datatable
    deleteSelectedUser() {
        if (this.selectedUser != null) {
        
            swal({
                title: 'Do you really want to delete',
                text: '' + this.selectedUser.firstName,
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    this.userService.deleteUser(this.selectedUser.id);
                    this.getAllUsers(this.page, this.size, this.sort, this.sortDir);
                    swal(
                        'Deleted!',
                        '<strong>' + this.selectedUser.firstName + ' </strong> has been deleted.',
                        'success'
                    )
                }
            }).catch(this.showErrorAlert);
        
        }
        
    }

    //enable delete and update buttons if an user is seleced
    onRowSelect(event) {
        this.dysplayButton = false;
    }

    //Retrieve all existing users from data base and catch errors if exist
    getAllUsers(page: number, size: number, sort: string, sortDir: string) {
        this.userService.getAllUsers(page, size, sort, sortDir)
            .then(results => {
                this.userList = results['content'];
                this.totalPages = results['totalPages'];
                this.totalElements = results['totalElements']
            })
            .catch(this.showErrorAlert);
    }

    //Launch an alert when something went wrong
    showErrorAlert() {
        swal({
            type: 'error',
            title: 'Oops...',
            text: 'Something went wrong!'
        })
    }

    //determine all dates which between the selected dates on spinner
    onYearChange(event, dt) {
        if (this.yearTimeout) {
            clearTimeout(this.yearTimeout);
        }

        this.yearTimeout = setTimeout(() => {
            dt.filter(event.value, 'year', 'gt');
        }, 250);
    }

    //Activate or deactivate the selected user
    toggleActivateUser() {
        if (this.selectedUser != null) {
            this.userService.toggleActivate(this.selectedUser.id)
                .catch(this.showErrorAlert);

            this.getAllUsers(this.page, this.size, this.sort, this.sortDir);
        }

    }

}