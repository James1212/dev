import { Component } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LoginService } from '../../../services/login.service';


@Component({
  templateUrl: 'login.component.html',
  styleUrls:['login.component.css'],
  providers:[LoginService]
})
export class LoginComponent {

  rForm: FormGroup;

  constructor(private fb: FormBuilder) {
    this.rForm = fb.group({
      'email': [null, [Validators.email, Validators.email]],
      'password': [null, Validators.compose([Validators.required, Validators.minLength(6)])],
    })
  }

}
