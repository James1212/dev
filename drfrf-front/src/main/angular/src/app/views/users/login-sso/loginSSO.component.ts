import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { LoginService } from '../../../services/login.service';
import { UserSession } from '../../../models/UserSession';
import { Router } from '@angular/router';


@Component({
  templateUrl: 'loginSSO.component.html',
  styleUrls: ['loginSSO.component.css'],
  providers: [LoginService]
})
export class LoginSSOComponent implements OnInit {


  constructor(
    private loginService: LoginService,
    private router: Router) { }

  ngOnInit() {
    this.loginBySso();
  }

  loginBySso() {
    this.loginService.loginBySso()
      .subscribe(
        (res: UserSession) => {
          if (res.idUtilisateur) {
            sessionStorage.setItem('login', res.login);
            sessionStorage.setItem('token', res.token);
            sessionStorage.setItem('fullName', res.firstName + ' ' + res.lastName);
            sessionStorage.setItem('codeProfile', res.codeProfil);
            sessionStorage.setItem('idUser', res.idUtilisateur + '');
            this.router.navigate(['/']);
          } else {
            this.router.navigate(['/403']);
          }
        }, error => {
          this.router.navigate(['/403']);
        }
      );
  }
}
