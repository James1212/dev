import { Component, OnInit } from "@angular/core";
import { User } from "../../../models/user";
import { FormBuilder, FormGroup, Validators, NgForm, PatternValidator } from '@angular/forms';
import { Role } from "../../../models/role";;
import { RegisterFormService } from "../../../services/register-form.service";
import swal from 'sweetalert2';

@Component({
    templateUrl: './register-form.component.html',
    styleUrls: ['./register-form.component.css'],
    providers: [RegisterFormService]
})
export class RegisterFormComponent implements OnInit {
    existingRoles: Role[];
    filteredRolesMultiple: Role[];
    rForm: FormGroup;
    file: File = null;

    //constructor of the component
    constructor(private fb: FormBuilder, private service: RegisterFormService) {
        this.rForm = fb.group({
            'firstName': [null, Validators.required],
            'lastName': [null, Validators.required],
            'email': [null, Validators.compose([Validators.email, Validators.required])],
            'password': [null, Validators.compose([Validators.required, Validators.minLength(6)])],
            'roles': [null, Validators.compose([Validators.required, Validators.minLength(1)])],

        })
    }

    //Retrieves all existing roles from database 
    ngOnInit() {
        this.get_all_roles();
    }

    //determine the roles which are corresponding to the role filer
    filterRoleMultiple(event) {
        let query = event.query;
        this.filteredRolesMultiple = this.filterRole(query, this.existingRoles);

    }

    filterRole(query, roles: Role[]): Role[] {
        //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
        let filtered: Role[] = [];
        for (let i = 0; i < roles.length; i++) {
            let role = roles[i];
            if (role.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(role);
            }
        }
        return filtered;
    }

    onSelect(event) {

        console.log(this.file);
        // if (event.target.files && event.target.files[0]) {
        //     let file = event.target.files[0];
        //     let newFile;
        //     let fr = new FileReader();
        //     fr.onload = (event: any) => {
        //         let base64 = event.target.result
        //         let img = base64.split(',')[1]
        //         let blob = new Blob([window.atob(img)], { type: 'image/jpeg' })
        //     }
        // }
    }

    //create a new user when the form is submitted
    create_user(newUser: User, userForm: NgForm) {
        this.service.create_user(newUser)
            .then(createTodo => {
                swal(
                    'successful operation!',
                    '<strong>' + newUser.firstName + ' </strong>is added successfully',
                    'success'
                );
                this.rForm.reset;
                userForm.reset()
            });
    }

    //get all existing roles using the service api 
    get_all_roles() {
        this.service.get_all_roles()
            .then(results => this.existingRoles = results);
    }


}