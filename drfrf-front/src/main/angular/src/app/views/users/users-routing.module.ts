import { NgModule } from '@angular/core';
import { Routes,RouterModule } from '@angular/router';
import { RegisterFormComponent } from './register/register-form.component';
import { UserListComponent } from './details/user-list.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Users'
    },
    children: [
      {
        path: 'register',
        component: RegisterFormComponent,
        data: {
          title: 'Registration Form'
        }
      },
      {
        path: 'list',
        component: UserListComponent,
        data: {
          title: 'Users list'
        }
      }]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule {}
