import { Injectable, ErrorHandler } from "@angular/core";
import { Observable } from 'rxjs/';

@Injectable()
export class CustomErrorHandler implements ErrorHandler {
    
   // Error handling
   handleError(error: Response | any): Observable<any> {
    console.error(error.message || error);
    return Observable.throw(error);
  }
}