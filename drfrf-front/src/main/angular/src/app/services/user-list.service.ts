import { Injectable } from "@angular/core";
import { Http, Headers } from "@angular/http";
import { User } from "../models/user";
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserListService {

    //the base url of the server side (back end)
    private baseUrl = 'http://localhost:8080';
    
    constructor(private http: Http) {}

    getAllUsers(page: number, size: number, sort: string, sortDir: string): Promise<User[]> {
        return this.http.get(this.baseUrl + '/users/?size=' + size + '&page=' + page + '&sort=' + sort + '&sortDir=' + sortDir)
            .toPromise()
            .then(response => response.json() as User[]);
    }

    //delete selected user from the datatable
    deleteUser(id: number): Promise<any> {
        return this.http.delete(this.baseUrl + '/users/' + id)
            .toPromise();
    }

    //delete selected user from the datatable
    toggleActivate(id: number): Promise<any> {
        return this.http.patch(this.baseUrl + '/users/toggleActivate/' + id, id)
            .toPromise();
    }

}