import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Repositories } from "../models/repositories";

//import { HttpHeaders } from "@angular/common/http";

@Injectable()
export class GeneratorService {

    private baseUrl = 'http://localhost:8080';

    constructor(private http: Http) { }

    generate(command: String): Promise<string> {
        return this.http.post(this.baseUrl + '/users/generator', command)
            .toPromise()
            .then(code => code.toString() as string);
    }

    pushSourceCode(repository : Repositories) : Promise<any>{                
        return this.http.post(this.baseUrl + '/git_api/', repository)
        .toPromise()
        .then(result => console.log(result));
    }
}                                                                                                                                                                                                                                                                                       
