import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Role } from "../models/role";
import { User } from "../models/user";

@Injectable()
export class RegisterFormService{

    private baseUrl = 'http://localhost:8080';
    constructor(private http:Http){}

    get_all_roles(): Promise<Role[]>{
        return this.http.get(this.baseUrl + '/roles/')
      .toPromise()
      .then(response => response.json() as Role[]);
    }

    create_user(user : User):Promise<User>{
       return this.http.post(this.baseUrl + '/users/',user)
      .toPromise()
      .then(response => response.json() as User);
    }

}