import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { CustomErrorHandler } from "../exceptions/CustomErrorHandler";

@Injectable()
export class LoginService {

    private baseUrl = 'http://localhost:8080';

    constructor(private http: Http, private customErrorHandler: CustomErrorHandler) { }

    loginBySso() {
        return this.http.post(this.baseUrl + '/users/login-sso', null)
        .catch((error: any) => this.customErrorHandler.handleError(error));
    }
}