export const navigation = [
  {
    name: 'Dashboard',
    url: '/dashboard',
    icon: 'icon-speedometer',
    badge: {
      variant: 'info',
      text: 'NEW'
    }
  },
  {
    name: 'Users',
    url: '/users',
    icon: 'icon-people',
    children: [
      {
        name: 'Add a new user',
        url: '/users/register',
        icon: 'icon-user-follow'
      },
      {
        name: 'Details',
        url: '/users/list',
        icon: 'icon-list'
      }
     ]
  }
];
