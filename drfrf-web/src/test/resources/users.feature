#Author: h.aitoubouhou@bouygues-construction.com
Feature: User service features

  Scenario: User makes call to GET /users/
    When User makes call to /users/
    Then User receives status code 200
    And User receives 47 elements

  Scenario: User makes call to PATCH /users/
    When User try to activate or deactivate an existing user with 12 as ID
    Then User receives status code of 200

  Scenario: User makes call to GET /users/send
    When User makes call to /users/send
    Then User receives an email and status code of 200
     
