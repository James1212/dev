package com.test.web.controller.cucumber;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.ResponseErrorHandler;
import org.springframework.web.client.RestTemplate;

import com.test.business.CoreApplication;
import com.test.business.service.dto.UserDto;
import com.test.web.WebopsWebApplication;

@ContextConfiguration(classes = { WebopsWebApplication.class, CoreApplication.class })
@WebAppConfiguration
// @SpringBootTest(classes = WebopsWebApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpringFunctionalTest {

    @Autowired
    protected RestTemplate restTemplate;

    static ResponseResults latestResponse = null;

    void executeGet(final String url) throws IOException {
        final Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        final HeaderSettingRequestCallback requestCallback = new HeaderSettingRequestCallback(headers);
        final ResponseResultErrorHandler errorHandler = new ResponseResultErrorHandler();
        restTemplate.setErrorHandler(errorHandler);
        latestResponse = restTemplate.execute(url, HttpMethod.GET, requestCallback, response -> {
            if (errorHandler.hadError) {
                return errorHandler.getResults();
            } else {
                return new ResponseResults(response);
            }
        });
    }

    void executePost(final String url, final UserDto userDto) throws IOException {
        final Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        final HeaderSettingRequestCallback requestCallback = new HeaderSettingRequestCallback(headers);
        final ResponseResultErrorHandler errorHandler = new ResponseResultErrorHandler();
        restTemplate.setErrorHandler(errorHandler);
        latestResponse = restTemplate.execute("http://localhost:8080/users" + url, HttpMethod.POST, requestCallback, response -> {
            if (errorHandler.hadError) {
                return errorHandler.getResults();
            } else {
                return new ResponseResults(response);
            }
        }, userDto);
    }

    private class ResponseResultErrorHandler implements ResponseErrorHandler {
        private ResponseResults results = null;
        private Boolean hadError = false;

        private ResponseResults getResults() {
            return results;
        }

        @Override
        public boolean hasError(final ClientHttpResponse response) throws IOException {
            hadError = response.getRawStatusCode() >= 400;
            return hadError;
        }

        @Override
        public void handleError(final ClientHttpResponse response) throws IOException {
            results = new ResponseResults(response);
        }
    }
}
