package com.test.web.controller.cucumber;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.test.business.CoreApplication;
import com.test.web.WebopsWebApplication;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { WebopsWebApplication.class, CoreApplication.class }, loader = SpringBootContextLoader.class)
@AutoConfigureMockMvc
@WebAppConfiguration
public class FunctionalStepsDef {

    // private static final Logger LOGGER = LoggerFactory.getLogger(FunctionalStepsDef.class);

    @Autowired
    private MockMvc mockMvc;

    private MvcResult result;

    @When("^User makes call to /users/$")
    public void user_makes_call_to_users() throws Throwable {
        result = mockMvc.perform(get("/users/")).andDo(print()).andExpect(status().isOk()).andReturn();
    }

    @Then("^User receives status code (\\d+)$")
    public void user_receives_status_code(final int statusCode) throws Throwable {
        assertEquals(result.getResponse().getStatus(), statusCode);
    }

    @Then("^User receives (\\d+) elements$")
    public void user_receives_elements(final int size) throws Throwable {
        mockMvc.perform(get("/users/")).andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$.totalElements").value(size));
    }

    @When("^User try to activate or deactivate an existing user with (\\d+) as ID$")
    public void user_try_to_activate_or_deactivate_an_existing_user_with_as_ID(final int id) throws Throwable {
        result = mockMvc.perform(MockMvcRequestBuilders.patch("/users/toggleActivate/{id}", id)).andReturn();

    }

    @Then("^User receives status code of (\\d+)$")
    public void the_admin_receives_status_code_of(final int statusCode) throws Throwable {
        assertEquals(result.getResponse().getStatus(), statusCode);
    }

    @When("^User makes call to /users/send$")
    public void user_makes_call_to_users_send() throws Throwable {
        result = mockMvc.perform(get("/users/send")).andReturn();
    }

    @Then("^User receives an email and status code of (\\d+)$")
    public void user_receives_an_email(final int statusCode) throws Throwable {
        assertEquals(result.getResponse().getStatus(), statusCode);
    }

}
