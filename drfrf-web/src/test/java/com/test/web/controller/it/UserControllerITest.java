package com.test.web.controller.it;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.test.business.CoreApplication;
import com.test.business.entity.User;
import com.test.web.WebopsWebApplication;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author h.aitoubouhou
 *
 *         Integration test for web layer
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = { WebopsWebApplication.class, CoreApplication.class })
@AutoConfigureMockMvc
public class UserControllerITest {

    @Autowired
    MockMvc mockMvc;

    /**
     * test activate or deactivate an existing user
     *
     * @throws Exception
     *             launched when something went wrong
     */
    @Test
    public void testToggleActivateUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/users/toggleActivate/{id}", 12L)).andDo(print()).andExpect(status().isOk());
    }

    /**
     * test activate or deactivate inexistent user
     *
     * @throws Exception
     *             launched when something went wrong
     */
    @Test(expected = Exception.class)
    public void testToggleActivateInexistentUser() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.patch("/users/toggleActivate/{id}", 132L)).andDo(print()).andExpect(status().isOk());
    }

    /**
     * check receiving the right number of users
     *
     * @throws Exception
     *             launched when something went wrong
     */
    @Test
    public void testGetAllUsers() throws Exception {
        mockMvc.perform(get("/users/")).andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$.totalElements").value(47));

    }

    /**
     * test creating a new user
     *
     * @throws Exception
     *             launched when something went wrong
     */
    @Ignore
    @Test
    public void testCreateUser() throws Exception {
        final User user = new User();

        final MvcResult result = mockMvc
                .perform(post("/users/").contentType(MediaType.APPLICATION_JSON).content(asJsonString(user)).accept(MediaType.APPLICATION_JSON)).andDo(print())
                .andReturn();

        Assert.assertNotNull(result);
    }

    /**
     * test remove an existing user
     *
     * @throws Exception
     */
    @Test
    public void testRemoveUser() throws Exception {

        mockMvc.perform(delete("/users/{id}", 11L).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

    }

    /**
     * test remove inexistent user
     *
     * @throws Exception
     */
    @Test(expected = Exception.class)
    public void testRemoveInexistentUser() throws Exception {
        mockMvc.perform(delete("/users/{id}", 0L).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
    }

    /**
     * test sending mail
     *
     * @throws Exception
     */
    @Test
    public void testSendMail() throws Exception {
        mockMvc.perform(get("/users/send")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Email sended, check your email box")));
    }

    @Test
    public void testUpdateExistingUser() throws Exception {
        final User user = new User();
        mockMvc.perform(put("/users/{id}", 12L).contentType(MediaType.APPLICATION_JSON).content(asJsonString(user))).andDo(print()).andExpect(status().isOk());
    }

    /**
     * Serialize Java objects into JSON string
     *
     * @param obj
     *            to serialize
     * @return JSON string
     */
    public static String asJsonString(final Object obj) throws Exception {

        return new ObjectMapper().writeValueAsString(obj);

    }
}