package com.test.web.controller.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(strict = true, monochrome = true, features = "src/test/resources", glue = { "com.test.web.controller.cucumber" }, plugin = { "pretty",
        "html:target/cucumber" })
public class CucumberTest {
}
