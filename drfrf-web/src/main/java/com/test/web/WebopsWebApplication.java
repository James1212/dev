package com.test.web;

import org.springframework.boot.Banner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

import com.test.business.CoreApplication;

/**
 * entry point of the application
 *
 * @author h.aitoubouhou
 *
 */
@SpringBootApplication
public class WebopsWebApplication extends SpringBootServletInitializer {

    public static void main(final String[] args) {
        new SpringApplicationBuilder().bannerMode(Banner.Mode.CONSOLE).sources(CoreApplication.class, WebopsWebApplication.class).run(args);
    }

}