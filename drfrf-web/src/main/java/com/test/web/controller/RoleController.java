package com.test.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.business.entity.Role;
import com.test.business.repo.RoleRepository;

@RestController
@RequestMapping(value = "/roles")
@CrossOrigin(origins = "http://localhost:4200")
public class RoleController {

    @Autowired
    private RoleRepository roleRepository;

    @GetMapping(value = "/")
    public List<Role> get_all_roles() {
        return roleRepository.findAll();
    }
}
