package com.test.web.controller;

import java.rmi.RemoteException;
import java.security.Principal;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.business.entity.User;
import com.test.business.exceptions.NotFoundException;
import com.test.business.exceptions.UnauthorizedException;
import com.test.business.model.Mail;
import com.test.business.service.dto.UserDto;
import com.test.business.service.dto.UserSessionDto;
import com.test.business.service.impl.MailService;
import com.test.business.service.impl.UserService;

/**
 * @author h.aitoubouhou
 *
 */
@RestController
@RequestMapping(value = "/users")
@CrossOrigin(origins = "http://localhost:4200") // FIXME activate only on dev mode
public class UserController {

    /**
     * Injecting the user service layer
     */
    @Autowired
    UserService userService;

    /**
     * Injecting the mail service layer
     */
    @Autowired
    MailService mailService;

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    /**
     * activate or deactivate user's status
     *
     * @param id
     *            of a user
     * @throws NotFoundException
     *             if the giving id is not found
     */
    @PatchMapping("/toggleActivate/{id}")
    public void toggle_activate(@PathVariable(value = "id") final Long id) throws NotFoundException {
        userService.toggleActivateUser(id);
    }

    /**
     * @param pageRequest
     * @return the list of users on pageable format
     */
    @GetMapping(value = "/")
    public Page<User> get_users(final Pageable pageRequest) {
        return userService.getAllUsers(pageRequest);
    }

    /**
     * @param the
     *            user to create
     * @return the user has just been created
     */
    @PostMapping(value = "/")
    public User create_user(@Valid @RequestBody final UserDto userDto) {
        return userService.createUser(userDto);
    }

    /**
     * delete an existing user
     *
     * @param id
     *            of the user to delete
     * @throws NotFoundException
     *             if the giving id is not exist
     */
    @DeleteMapping(value = "/{id}")
    public void remove_user(@PathVariable(value = "id") final Long id) throws NotFoundException {
        userService.deleteUser(id);
    }

    /**
     * update an existing user
     *
     * @param id
     *            of the user to update
     * @param newUser
     * @throws NotFoundException
     *             launched when no user found
     */
    @PutMapping(value = "/{id}")
    public void update_user(@PathVariable(value = "id") final Long id, @Valid @RequestBody final UserDto newUser) throws NotFoundException {
        userService.updateUser(id, newUser);
    }

    /**
     * Send an email using TEST MAIL SERVER TOOL
     *
     * @throws Exception
     *             when something went wrong
     */
    @GetMapping(value = "/send")
    public String send_email() throws Exception {
        final Mail mail = new Mail();
        mail.setMailFrom("javabycode@gmail.com");
        mail.setMailTo("aitoubouhouhafid@gmail.com");
        mail.setMailContent(
                "Le Lorem Ipsum est simplement du faux texte employé dans lacomposition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500");
        mail.setMailSubject("FreeMarcker");

        final Map<String, Object> model = new HashMap<>();
        model.put("content", mail.getMailContent());
        model.put("firstName", "David");
        model.put("lastName", "Pham");
        model.put("location", "Casablanca");
        model.put("signature", "www.bycn.com");
        mail.setModel(model);
        mailService.sendEmail(mail);

        return "Email sended, check your email box ";
    }

    /**
     * Generate a maven project using a custm archetype
     * 
     * @param command
     * @return
     */
    @PostMapping(value = "/generator")
    public int custom_archetype(@Valid @RequestBody final String command) {
        LOGGER.info("query ===> " + command);
        return userService.customArchetype(command);

    }

    /**
     * @param request
     * @return
     * @throws RemoteException
     * @throws UnauthorizedException
     */
    @PostMapping("/login-sso")
    public ResponseEntity<UserSessionDto> login(final HttpServletRequest request) throws RemoteException, UnauthorizedException {

        final String login = getUserLoginFromPrincipal(request);
        ResponseEntity<UserSessionDto> result = null;

        if (StringUtils.isNotEmpty(login)) {
            // SSo Mode
            final UserSessionDto loggedUser = userService.findUserBySSO(login);

            result = authorizeUserAccessToWebops(loggedUser);
        } else {
            // No SSO
            result = new ResponseEntity<>(new UserSessionDto(), HttpStatus.UNAUTHORIZED);
        }

        return result;
    }

    private String getUserLoginFromPrincipal(final HttpServletRequest request) {
        // Get the account via SSO
        final Principal principal = request.getUserPrincipal();
        String login = StringUtils.EMPTY;
        LOGGER.debug(String.format("LOGIN BY principal : %s", principal));
        if (principal != null) {
            final String name = principal.getName();
            if (name != null && !name.isEmpty() && name.indexOf('@') != -1) {
                login = name.substring(0, name.indexOf('@'));
                LOGGER.debug(String.format("LOGIN BY SSO: login = %s", login));
            }
        }
        return login;
    }

    private ResponseEntity<UserSessionDto> authorizeUserAccessToWebops(final UserSessionDto loggedUser) {

        final String token = userService.generateToken(loggedUser);
        loggedUser.setToken(token);
        LOGGER.info(String.format("Login succeeded for user : %s", loggedUser.getLogin()));
        LOGGER.info(String.format("User Role is : %s", loggedUser.getCodeProfil()));
        LOGGER.info(String.format("Generated token : %s", loggedUser.getToken()));

        return new ResponseEntity<>(loggedUser, HttpStatus.OK);
    }

}