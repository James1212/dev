package com.test.web.controller;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.test.business.model.Repositories;
import com.test.business.service.impl.GitService;

@RestController
@RequestMapping(value = "/git_api")
@CrossOrigin(origins = "http://localhost:4200")
public class GitController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GitController.class);

    @Autowired
    GitService gitService;

    @PostMapping(value = "/")
    public void push_source_code(@Valid @RequestBody final Repositories repository) throws URISyntaxException, IOException {
        GitService.pushSourceCode(repository);
        LOGGER.info("======= PUSH =======");
    }

}
