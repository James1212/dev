package com.test.business.specification;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.test.context.junit4.SpringRunner;

import com.test.business.entity.User;
import com.test.business.repo.UserRepository;

/**
 * @author h.aitoubouhou
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class UserSpecificationTest {

    @Autowired
    private UserRepository userRepository;

    private User user1 = new User();
    private User user2 = new User();
    private SearchCriteria criteria1 = new SearchCriteria();
    private SearchCriteria criteria2 = new SearchCriteria();

    @Before
    public void setup() {
        user1.setFirstName("John");
        user1.setLastName("narcos");

        userRepository.save(user1);

        user2.setFirstName("narcos");
        user2.setLastName("narcos");

        userRepository.save(user2);

        criteria1.setKey("lastName");
        criteria1.setOperation(":");
        criteria1.setValue("narcos");

        criteria2.setKey("firstName");
        criteria2.setOperation(":");
        criteria2.setValue("narcos");
    }

    @Test
    public void find_a_new_added_user() {

        // Arrange
        final UserSpecification spec1 = new UserSpecification(criteria1);
        final UserSpecification spec2 = new UserSpecification(criteria2);

        // Act
        final User user = userRepository.findOne(Specifications.where(spec1).and(spec2));

        // Assert
        assertNotNull(user);
    }

}
