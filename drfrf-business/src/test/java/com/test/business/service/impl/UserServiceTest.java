package com.test.business.service.impl;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import com.test.business.entity.User;
import com.test.business.exceptions.NotFoundException;
import com.test.business.repo.UserRepository;
import com.test.business.service.dto.UserDto;
import com.test.business.service.mapper.impl.UserMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper mapper;

    private User user;

    /**
     * Initialization of Mocks
     */
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    /**
     * user not found exception
     *
     * @throws NotFoundException
     *             when no user found
     */

    @Test(expected = NotFoundException.class)
    public void testToggleActivateInexistantUser() throws NotFoundException {
        // Arrange
        final Long id = 0L;
        Mockito.when(userRepository.findOne(id)).thenReturn(null);

        // Act
        userService.toggleActivateUser(id);

        // Assert
        // Exception expected

    }

    /**
     * Activate and deactivate user's state
     *
     * @throws NotFoundException
     *             when no user found
     */

    @Test
    public void testToggleActivateUser() throws NotFoundException {
        // Arrange
        final Long id = 2L;
        Mockito.when(userRepository.findOne(id)).then(invocation -> {
            user = new User();
            user.setActive(true);
            return user;
        });

        // Act
        userService.toggleActivateUser(id);

        // Assert
        Assert.assertEquals(false, user.isActive());
    }

    /**
     * Create a new user
     */
    @Ignore
    @Test
    public void testCreateUser() {
        // Arrange
        final UserDto newUser = new UserDto();

        Mockito.when(mapper.asUser(newUser)).thenReturn(new User());
        Mockito.when(userRepository.save(Matchers.any(User.class))).thenReturn(new User());
        // Act
        final User result = userService.createUser(newUser);

        // Assert
        Assert.assertNotNull(result);
    }

    /**
     * Delete an existing user
     *
     * @throws NotFoundException
     *             when no user found to delete
     */

    @Ignore
    @Test
    public void testDeleteUser() throws NotFoundException {
        // Arrange
        final Long id = 2L;
        Mockito.when(userRepository.findOne(id)).thenReturn(new User());

        // Act
        userService.deleteUser(id);

        // Assert
        Mockito.verify(userRepository, Mockito.times(1)).delete(Matchers.eq(id));
    }

    /**
     * Delete inexistent user
     *
     * @throws NotFoundException
     *             when no user found to delete
     */

    @Test(expected = NotFoundException.class)
    public void testDeleteInexistentUser() throws NotFoundException {
        // Arrange
        final Long id = 0L;
        Mockito.when(userRepository.findOne(id)).thenReturn(null);

        // Act
        userService.deleteUser(id);

        // Assert
        // Exception expected
    }

    /**
     * Update an inexistent user
     *
     * @throws NotFoundException
     *             when no user found
     */

    @Test(expected = NotFoundException.class)
    public void testUpdateInexistentUser() throws NotFoundException {
        // Arrange
        final Long id = 0L;
        final UserDto newUserDto = new UserDto();
        Mockito.when(userRepository.findOne(id)).thenReturn(null);

        // Act
        userService.updateUser(id, newUserDto);

        // Assert
        // Exception expected
    }

    /**
     * Update an existing user
     *
     * @throws NotFoundException
     *             when no user found
     */

    @Test
    public void testUpdateUser() throws NotFoundException {
        // Arrange
        final Long id = 10L;
        final UserDto newUserDto = new UserDto();
        Mockito.when(userRepository.findOne(id)).thenReturn(new User());
        Mockito.when(mapper.asUser(newUserDto)).thenReturn(new User());

        // Act
        userService.updateUser(id, newUserDto);

        // Assert
        Assert.assertEquals(id, newUserDto.getId());
    }

    /**
     * test getting all users
     */
    @Test
    public void testGetAllUsers() {
        // Arrange
        final PageRequest pageable = new PageRequest(1, 3);
        final PageImpl<User> usersPage = new PageImpl<>(Arrays.asList(new User(), new User(), new User()), pageable, 10);

        Mockito.when(userRepository.findAll(pageable)).thenReturn(usersPage);

        // Act
        final Page<User> page = userService.getAllUsers(pageable);

        // Assert
        Assert.assertEquals(3, page.getContent().size());
        Assert.assertEquals(10, page.getTotalElements());
    }

}
