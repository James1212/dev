package com.test.business.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;

import java.util.HashMap;
import java.util.Map;

import javax.mail.internet.MimeMessage;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.test.context.junit4.SpringRunner;

import com.test.business.model.Mail;

import freemarker.template.Configuration;

/**
 * Unit test for MailService layer
 *
 * @author h.aitoubouhou
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MailServiceTest {

    @Autowired
    private MailService mailService;

    @Mock
    private JavaMailSender sender;

    @Mock
    private Configuration freemarkerConfig;

    private Mail mail = new Mail();

    @Before
    public void setup() {

        mail.setMailFrom("javabycode@gmail.com");
        mail.setMailTo("aitoubouhouhafid@gmail.com");
        mail.setMailContent(
                "Le Lorem Ipsum est simplement du faux texte employé dans lacomposition et la mise en page avant impression. Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500,");
        mail.setMailSubject("FreeMarcker");

        final Map<String, Object> model = new HashMap<>();
        model.put("content", mail.getMailContent());
        model.put("firstName", "David");
        model.put("lastName", "Pham");
        model.put("location", "Casablanca");
        model.put("signature", "www.bycn.com");
        mail.setModel(model);
    }

    /**
     * test messaging with a local server mail
     *
     * @throws Exception
     *             launched when something went wrong
     */
    @Test
    public void testSendingMail() throws Exception {
        // Arrange
        Mockito.doNothing().when(sender).send(any(MimeMessage.class));
        Mockito.doNothing().when(freemarkerConfig).setClassForTemplateLoading(eq(getClass()), anyString());
        Mockito.when(freemarkerConfig.getTemplate(anyString())).then(invocation -> freemarkerConfig.getTemplate("email-template.ftl"));

        // Act
        mailService.sendEmail(mail);

        // Assert
        assert Boolean.TRUE;
    }

}
