/**
 *
 */
package com.test.business.exceptions;

/**
 * Unauthorized Exception raised when user is not granted the access to WATT.
 *
 */
public class UnauthorizedException extends Exception {

    /** serialVersionUID */
    private static final long serialVersionUID = 1L;

    public static final String UNAUTHORIZED_ACCESS = "UNAUTHORIZED_ACCESS";

    /**
     * Constructeur
     */
    public UnauthorizedException() {
        super(UNAUTHORIZED_ACCESS);
    }

    /**
     * Construit une exception avec un message
     *
     * @param message
     *            description de l'erreur
     */
    public UnauthorizedException(final String message) {
        super(message);
    }

    /**
     * Construit une exception avec un message et la cause
     *
     * @param message
     *            description de l'erreur
     * @param cause
     *            exception
     */
    public UnauthorizedException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
