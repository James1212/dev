/**
 *
 */
package com.test.business.exceptions;

/**
 * Unauthorized Exception raised when user has failed in his authentication attempt.
 *
 */
public class AuthenticationFailedException extends RuntimeException {

    /** serialVersionUID */
    private static final long serialVersionUID = 1L;

    public static final String AUTHENTICATION_FAILED = "AUTHENTICATION_FAILED";

    /**
     * Constructeur
     */
    public AuthenticationFailedException() {
        super(AUTHENTICATION_FAILED);
    }

    /**
     * Construit une exception avec un message
     *
     * @param message
     *            description de l'erreur
     */
    public AuthenticationFailedException(final String message) {
        super(message);
    }

    /**
     * Construit une exception avec un message et la cause
     *
     * @param message
     *            description de l'erreur
     * @param cause
     *            exception
     */
    public AuthenticationFailedException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
