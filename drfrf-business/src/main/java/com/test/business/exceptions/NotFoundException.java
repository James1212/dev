package com.test.business.exceptions;

/**
 * Exception launched when requested user doesn't exist
 * 
 * @author h.aitoubouhou
 */
public class NotFoundException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * Constructor
     */
    public NotFoundException() {
        super();
    }

    /**
     * build an exception with a message
     *
     * @param message
     *            description of error
     * 
     */

    public NotFoundException(final String message) {
        super(message);
    }

    /**
     * build an exception with a message and a cause
     *
     * @param message
     *            description of error
     * @param cause
     *            exception
     */
    public NotFoundException(final String message, final Throwable cause) {
        super(message, cause);
    }

}
