package com.test.business.specification;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.test.business.entity.User;

/**
 * Specification JPA management service
 *
 * @author h.aitoubouhou
 *
 *         Search by Date of creation/modification, FirstName, LastName,
 *         Email,etc
 *
 */
public class UserSpecification implements Specification<User> {

	private SearchCriteria criteria;

	public UserSpecification(final SearchCriteria criteria) {
		this.criteria = criteria;
	}

	@Override
	public Predicate toPredicate(final Root<User> root, final CriteriaQuery<?> query, final CriteriaBuilder builder) {

		if (criteria.getOperation().equalsIgnoreCase(">")) {
			if (root.get(criteria.getKey()).getJavaType() == Date.class) {
				return builder.greaterThanOrEqualTo(root.<String>get(criteria.getKey()),
						criteria.getValue().toString());
			} else {
				return null;
			}

		} else if (criteria.getOperation().equalsIgnoreCase("<")) {
			if (root.get(criteria.getKey()).getJavaType() == Date.class) {
				return builder.greaterThanOrEqualTo(root.<String>get(criteria.getKey()),
						criteria.getValue().toString());
			} else {
				return null;
			}

		} else if (criteria.getOperation().equalsIgnoreCase(":")) {
			if (root.get(criteria.getKey()).getJavaType() == String.class) {
				return builder.like(root.<String>get(criteria.getKey()), "%" + criteria.getValue() + "%");
			} else {
				return builder.equal(root.get(criteria.getKey()), criteria.getValue());
			}
		}
		return null;
	}

}
