package com.test.business.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * describes the roles of user
 *
 * @author h.aitoubouhou
 *
 */
@Entity
@Data
@Table(name = "T_ROLES")
public class Role implements Serializable {

	private static final long serialVersionUID = 1L;

	/** Role's id */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "ID")
	Long id;

	/** Role's name */
	@NotNull
	@Column(name = "NAME")
	String name;

	/** Role's description */
	@Column(name = "DESCRIPTION")
	String description;
}
