package com.test.business.entity;

import java.io.Serializable;
import java.sql.Blob;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import lombok.Data;

/**
 * describes project's users
 *
 * @author h.aitoubouhou
 *
 */

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
@Table(name = "T_USERS")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    /** user's id */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    /** ARGOS ID */
    @Column(name = "ID_ARGOS")
    private String idArgos;

    /** user's firstName */
    @Column(name = "FIRSTNAME")
    private String firstName;

    /** user's lastName */
    @Column(name = "LASTNAME")
    private String lastName;

    /** Creation date */
    @Column(name = "CREATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createDate;

    /** last modification date */
    @Column(name = "UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updateDate;

    /** user's email */
    @Column(name = "EMAIL")
    private String email;

    /** user's login */
    @Column(name = "Login")
    private String login;

    /** user's photo */
    @Column(name = "PHOTO")
    private Blob photo;

    /** user's password */
    @Column(name = "PASSWORD")
    private String password;

    /** user's status */
    @Column(name = "ACTIVE", columnDefinition = "tinyint(1) default 0")
    private boolean active;

    /** the list of roles granted to the user */
    @ManyToMany
    @JoinTable(name = "USER_ROLE", joinColumns = @JoinColumn(name = "USER_ID", referencedColumnName = "ID"), inverseJoinColumns = @JoinColumn(name = "ROLE_ID", referencedColumnName = "ID"))
    private Set<Role> roles = new HashSet<>();

}
