package com.test.business;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

/**
 * Core layer configuration
 *
 * @author h.aitoubouhou
 */
@SpringBootApplication
@PropertySource("classpath:core.properties")
public class CoreApplication {

}