package com.test.business.constants;

/**
 * Application constants
 *
 */
public abstract class ApplicationConstants {

    /**
     * Patterns constants
     *
     */
    public static class Pattern {
        public static final String DEFAULT_BYCN_DOMAIN = "BYCN\\";
        public static final String DEFAULT_DATE = "dd/MM/yyyy";
        public static final int BYCN_DOMAIN_NAME_SIZE = 5;
        public static final String WBS_PARENT_PATTERN = "^[0-9a-zA-Z]+\\.[0-9a-zA-Z]+$";
    }

    /**
     * Values taken from table ACTION.
     */
    public static class Action {

        /** Creation. */
        public static final String CRT = "CRT";

        /** Integration. */
        //
        public static final String INT = "INT";

        /** Modification. */
        public static final String MDF = "MDF";
    }

    /**
     * Values taken from table STATUT
     *
     */
    public static class Statut {
        /** Creation/modification . */
        public static final String CRM = "CRM";

        /** A integrer. */
        public static final String AIN = "AIN";

        /** Integration . */
        public static final String INT = "INT";
    }

    public static class TypeEvaluation {
        public static final String SEC = "SEC";

        public static final String NSE = "NSE";
    }

    public static class TypeTs {
        public static final String PMICE = "PMICE";

        public static final String PMI = "PMI";

        public static final String CE = "CE";

        public static final String INFLA = "INFLA";

        public static final String BUFF = "BUFF";
    }

    public static class TypeStatut {
        public static final String EXW = "EXW";

        public static final String INT = "INT";

        public static final String ADQ = "ADQ";

        public static final String MOW = "MOW";
    }

    public static class PortailPartenairesWS {
        public static String PORTAIL_PARTNER_ERROR_AUTH_PARAM = "error_auth_param";

        public static String PORTAIL_PARTNER_ERROR_TECHNICAL = "error_technical";
    }

    public static class EXCEL {
        public static String MIME_TYPE_XLSX = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

        public static String AW_FR = "Liste_TS";

        public static String AW_EN = "AW_List";

        public static String BUDGETS_FR = "Liste_SAP";

        public static String BUDGETS_EN = "SAP_List";
    }
}
