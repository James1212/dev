package com.test.business.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.test.business.entity.User;

/**
 * Repository interface for user's management
 *
 * @author h.aitoubouhou
 *
 */

@Repository
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    /**
     *
     * @param lastName
     * @return a user who his names contains the lastName given as parameter
     */
    @Query("SELECT user FROM User user WHERE user.lastName like %?1%")
    public User findByLastNameContaining(String lastName);

    /**
     * Using a method query
     *
     * @param firstName
     * @return a list of users whose names contains the lastName given as parameter
     */
    public List<User> findByFirstNameContaining(String name);

    /**
     * Find user by his email
     *
     * @param email
     *            of user
     * @return user who has the giving email
     */
    @Query("SELECT user FROM User user WHERE user.email like %?1%")
    public Optional<User> findByEmail(String email);

    /**
     * Find user by his login
     *
     * @param email
     *            of user
     * @return user who has the giving login
     */
    @Query("SELECT user FROM User user WHERE user.login like %?1% AND (user.active = 'True' OR user.active = 1)")
    public User findUserByLogin(String login);

}
