package com.test.business.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.test.business.entity.ProfilUser;

@Repository
public interface ProfilUserRepository extends JpaRepository<ProfilUser, Integer> {

    /**
     * Find user's profil by utilisateur id.
     *
     * @param idUtilisateur
     *            the id utilisateur
     * @return the profil utilisateur
     */
    @Transactional(readOnly = true)
    @Query("SELECT pu FROM ProfilUser pu LEFT JOIN FETCH pu.user u " + "LEFT JOIN FETCH pu.profil p WHERE u.id=:id")
    ProfilUser findByUser(@Param("id") final Long id);

}
