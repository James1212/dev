package com.test.business.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.test.business.entity.Role;

/**
 * RoleRepository interface
 *
 * @author h.aitoubouhou
 *
 */

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    public Role findByName(String name);

}