package com.test.business.model;

import lombok.Data;

@Data
public class Repositories {

    private String url;
    private String username;
    private String password;
    private String directoryName;

}
