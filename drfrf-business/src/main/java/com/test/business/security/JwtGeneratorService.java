package com.test.business.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.test.business.service.dto.UserSessionDto;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

/**
 * This service generates tokens and verify them
 *
 * @author Z.DRISSI
 */
@Component
public class JwtGeneratorService {

    /** Secret key provider service */
    @Autowired
    private SecretKeyProvider secretKeyProvider;

    /**
     * Generates a key based on the parameter
     *
     * @param user
     *            user infos
     * @return a generated token
     */
    public String generate(final UserSessionDto user) {

        final byte[] secretKey = secretKeyProvider.getKey();

        final Claims claims = Jwts.claims().setSubject(user.getLogin());

        claims.put("id", String.valueOf(user.getId()));
        claims.put("role", user.getCodeProfil());

        return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, secretKey).compact();
    }

    /**
     * Verifies the token
     *
     * @param token
     *            the token value to verify
     * @return user infos
     */
    public UserSessionDto verify(final String token) {

        final byte[] secretKey = secretKeyProvider.getKey();

        final Claims body = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody();

        final UserSessionDto user = new UserSessionDto();

        user.setLogin(body.getSubject());
        user.setId(Long.parseLong(body.get("id").toString()));
        user.setCodeProfil((String) body.get("role"));

        return user;
    }
}
