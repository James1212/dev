package com.test.business.security;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import com.test.business.service.dto.UserSessionDto;

/**
 * The class that represents an authenticated user
 *
 * @author Z.DRISSI
 */
public class JwtAuthenticatedUser implements Authentication {

    /** serialVersionUID */
    private static final long serialVersionUID = 2431546238541929863L;

    /** The connected user */
    private final UserSessionDto user;

    /**
     * Constructor for user
     *
     * @param user
     *            the user
     */
    public JwtAuthenticatedUser(final UserSessionDto user) {
        this.user = user;
    }

    /**
     * @see java.security.Principal#getName()
     */
    @Override
    public String getName() {
        return user.getLogin();
    }

    /**
     * @see org.springframework.security.core.Authentication#getAuthorities()
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return AuthorityUtils.commaSeparatedStringToAuthorityList(user.getCodeProfil());
    }

    /**
     * @see org.springframework.security.core.Authentication#getCredentials()
     */
    @Override
    public Object getCredentials() {
        return null;
    }

    /**
     * @see org.springframework.security.core.Authentication#getDetails()
     */
    @Override
    public Object getDetails() {
        return null;
    }

    /**
     * @see org.springframework.security.core.Authentication#getPrincipal()
     */
    @Override
    public Object getPrincipal() {
        return null;
    }

    /**
     * @see org.springframework.security.core.Authentication#isAuthenticated()
     */
    @Override
    public boolean isAuthenticated() {
        return true;
    }

    /**
     * @see org.springframework.security.core.Authentication#setAuthenticated(boolean)
     */
    @Override
    public void setAuthenticated(final boolean isAuthenticated) throws IllegalArgumentException {
    }
}
