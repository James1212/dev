package com.test.business.security;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Retrieves the key
 *
 * @author Z.DRISSI
 */
@Component
public class SecretKeyProvider {

    /** Logger Object */
    private static final Logger LOGGER = LoggerFactory.getLogger(SecretKeyProvider.class);

    /** The default key value */
    private static final String DEFAULT_KEY = "1e2eb1eb47d9d18cf8c901268bfc9ede";

    /**
     * Reads the key and returns it, it returns the default key if it cannot read the key
     *
     * @return key value
     */
    public byte[] getKey() {

        try {
            return Files.readAllBytes(Paths.get(this.getClass().getResource("/security/jwt.key").toURI()));
        } catch (IOException | URISyntaxException e) {
            LOGGER.warn("Couldn't load the key, using the default key instead", e);
        }

        return DEFAULT_KEY.getBytes();
    }
}
