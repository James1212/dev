package com.test.business.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import com.test.business.exceptions.AuthenticationFailedException;
import com.test.business.service.dto.UserSessionDto;

import io.jsonwebtoken.JwtException;

/**
 * JWT Authentication Provider
 *
 * @author Z.DRISSI
 */
@Component
public class JwtAuthenticationProvider implements AuthenticationProvider {

    /** Logger Object */
    private static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthenticationProvider.class);

    @Autowired
    private JwtGeneratorService jwtService;

    /**
     * @see org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider#authenticate(org.springframework.security.core.Authentication)
     */
    @Override
    public Authentication authenticate(final Authentication authentication) throws AuthenticationException {

        try {
            final String token = (String) authentication.getCredentials();

            LOGGER.info(">>>>>>> [token='{}'][class={}]", token, authentication.getClass());

            final UserSessionDto dto = jwtService.verify(token);

            return new JwtAuthenticatedUser(dto);
        } catch (final JwtException e) {
            throw new AuthenticationFailedException("Failed to verify token", e);
        }
    }

    /**
     * @see org.springframework.security.authentication.dao.AbstractUserDetailsAuthenticationProvider#supports(java.lang.Class)
     */
    @Override
    public boolean supports(final Class<?> authentication) {
        return JwtAuthToken.class.equals(authentication);
    }
}
