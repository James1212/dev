package com.test.business.security;

import java.util.Collection;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/**
 * Represents the Token
 *
 * @author Z.DRISSI
 */
public class JwtAuthToken implements Authentication {

    /** serialVersionUID */
    private static final long serialVersionUID = -4894226097144440518L;

    /** JWT Token */
    private final String token;

    /**
     * Constructor
     *
     * @param token
     *            token value
     */
    public JwtAuthToken(final String token) {
        this.token = token;
    }

    /**
     * @see org.springframework.security.core.Authentication#getAuthorities()
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    /**
     * @see org.springframework.security.core.Authentication#getCredentials()
     */
    @Override
    public Object getCredentials() {
        return token;
    }

    /**
     * @see org.springframework.security.core.Authentication#getDetails()
     */
    @Override
    public Object getDetails() {
        return null;
    }

    /**
     * @see org.springframework.security.core.Authentication#getPrincipal()
     */
    @Override
    public Object getPrincipal() {
        return null;
    }

    /**
     * @see org.springframework.security.core.Authentication#isAuthenticated()
     */
    @Override
    public boolean isAuthenticated() {
        return false;
    }

    /**
     * @see org.springframework.security.core.Authentication#setAuthenticated(boolean)
     */
    @Override
    public void setAuthenticated(final boolean isAuthenticated) throws IllegalArgumentException {

    }

    /**
     * @see java.security.Principal#getName()
     */
    @Override
    public String getName() {
        return null;
    }
}
