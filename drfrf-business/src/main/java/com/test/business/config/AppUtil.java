package com.test.business.config;

import java.io.IOException;
import java.util.Properties;

public class AppUtil {
    public static String getProperties(final String propertiesName) {
        final Properties prop = new Properties();
        try {
            prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("ws.properties"));
            return prop.getProperty(propertiesName);
        } catch (final IOException ex) {
            ex.printStackTrace();
        }
        return null;
    }

    public static String[] getLabels(final String lang, final String type) {
        final Properties prop = new Properties();

        final String fileName = (lang.equals("fr") ? "labels_fr" : "labels_en") + ".properties";

        try {
            prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName));

            if (type.equals("aw")) {
                final String[] results = { prop.getProperty("aw.no_buffer"), prop.getProperty("aw.no_inflation"), prop.getProperty("aw.no_cemar_pmi"),
                        prop.getProperty("aw.no_cemar_ce"), prop.getProperty("aw.desc"), prop.getProperty("aw.aw_type"), prop.getProperty("aw.stt_type"),
                        prop.getProperty("aw.eval_type"), prop.getProperty("aw.remark"), prop.getProperty("aw.currency"), prop.getProperty("aw.aw_amt"),
                        prop.getProperty("aw.fee_amt"), prop.getProperty("aw.fee_pct"), prop.getProperty("aw.aw_budget"), prop.getProperty("aw.wbs"),
                        prop.getProperty("aw.wbs_desc"), prop.getProperty("aw.stt"), prop.getProperty("aw.stt_date"), prop.getProperty("aw.aw_qty"),
                        prop.getProperty("aw.aw_unit"), prop.getProperty("aw.wbs_budget"), prop.getProperty("aw.res_code"), prop.getProperty("aw.res_desc"),
                        prop.getProperty("aw.res_qty"), prop.getProperty("aw.res_unit"), prop.getProperty("aw.res_unit_price"), prop.getProperty("aw.res_amt"),
                        prop.getProperty("aw.aw_qty_gap"), prop.getProperty("aw.res_qty_gap"), prop.getProperty("aw.unit_price_gap"),
                        prop.getProperty("aw.amt_gap"), prop.getProperty("aw.crt_date"), prop.getProperty("aw.crt_user"), prop.getProperty("aw.mdf_date"),
                        prop.getProperty("aw.mdf_user"), prop.getProperty("aw.int_date"), prop.getProperty("aw.int_user") };
                return results;
            } else if (type.equals("wbs")) {
                final String[] results = { prop.getProperty("wbs.wbs"), prop.getProperty("wbs.res_code"), prop.getProperty("wbs.res_desc"),
                        prop.getProperty("wbs.res_qty"), prop.getProperty("wbs.res_unit"), prop.getProperty("wbs.res_unit_price") };
                return results;
            }
        } catch (final IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static String getLabelByCode(final String lang, final String code) {
        final Properties prop = new Properties();

        final String fileName = (lang.equals("fr") ? "labels_fr" : "labels_en") + ".properties";

        try {
            prop.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(fileName));
            return prop.getProperty(code);
        } catch (final IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }
}
