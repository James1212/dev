package com.test.business.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Registers the DelegatingFilterProxy to use the springSecurityFilterChain before any other registered Filter.
 *
 * @author Z.DRISSI
 */
public class SecurityWebApplicationInitializer extends AbstractSecurityWebApplicationInitializer {

}
