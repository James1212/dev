package com.test.business.webservices.comunitypeople;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.rpc.ServiceException;

import org.apache.axis.utils.StringUtils;

import com.test.business.config.AppUtil;
import com.test.business.service.dto.ComunityPeopleUserDto;
import com.structis.argos.ws.WsArgosPort;
import com.structis.argos.ws.WsArgosPortServiceLocator;
import com.structis.argos.ws.WsArgosPortSoapBindingStub;
import com.structis.argos.ws.WsPersonneLightListResultType;
import com.structis.argos.ws.WsPersonneLightType;
import com.structis.argos.ws.WsPersonneResultType;

/**
 * The Class CommunicationComPeople used for calling WS of Comunity People.
 */
public class CommunicationComPeople {

    /** The binding. */
    private WsArgosPort binding;

    /**
     * Instantiates a new communication com people.
     *
     * @throws MalformedURLException
     *             the malformed URL exception
     * @throws ServiceException
     *             the service exception
     */
    public CommunicationComPeople() throws MalformedURLException, ServiceException {
        final String username = AppUtil.getProperties("comunityPeopleWs.username");
        final String password = AppUtil.getProperties("comunityPeopleWs.password");
        final String timeout = AppUtil.getProperties("comunityPeopleWs.timeout");
        final String url = AppUtil.getProperties("comunityPeopleWs.url");
        bindWS(username, password, timeout, url);
    }

    /**
     * Instantiates a new communication com people.
     *
     * @param username
     *            the username
     * @param password
     *            the password
     * @param timeout
     *            the timeout
     * @param url
     *            the url
     * @throws MalformedURLException
     *             the malformed URL exception
     * @throws ServiceException
     *             the service exception
     */
    public CommunicationComPeople(final String username, final String password, final String timeout, final String url)
            throws MalformedURLException, ServiceException {
        bindWS(username, password, timeout, url);
    }

    /**
     * Bind WS.
     *
     * @param username
     *            the username
     * @param password
     *            the password
     * @param timeout
     *            the timeout
     * @param url
     *            the url
     * @throws MalformedURLException
     *             the malformed URL exception
     * @throws ServiceException
     *             the service exception
     * @throws javax.xml.rpc.ServiceException
     */
    private void bindWS(final String username, final String password, final String timeout, final String url) throws MalformedURLException, ServiceException {
        final WsArgosPortServiceLocator locator = new WsArgosPortServiceLocator();
        binding = locator.getwsArgosPort(new URL(url));
        ((WsArgosPortSoapBindingStub) binding).setTimeout(Integer.parseInt(timeout));
        ((WsArgosPortSoapBindingStub) binding).setUsername(username);
        ((WsArgosPortSoapBindingStub) binding).setPassword(password);
    }

    /**
     * Check if account is valid.
     *
     * @param argosId
     *            the argos id
     * @return the boolean
     * @throws RemoteException
     *             the remote exception
     */
    public Boolean checkAccountIsValid(final String argosId) throws RemoteException {
        if (StringUtils.isEmpty(argosId)) {
            return false;
        }
        final WsPersonneResultType wsPersonneResultType = binding.getPersonneById(argosId);

        return wsPersonneResultType != null;
    }

    /**
     * Gets the list personne.
     *
     * @param nom
     *            the nom
     * @param prenom
     *            the prenom
     * @param email
     *            the email
     * @return the list personne
     * @throws RemoteException
     *             the remote exception
     */
    public List<ComunityPeopleUserDto> getListPersonne(final String nom, final String prenom, final String email) throws RemoteException {
        final List<ComunityPeopleUserDto> result = new ArrayList<>();
        if (nom == null && prenom == null && email == null || nom.isEmpty() && prenom.isEmpty() && email.isEmpty()) {
            return result;
        }
        final String searchString = buildSearchString(nom, prenom, email);
        final WsPersonneLightListResultType wsPersonList = binding.getListPersonneByString(searchString);
        final WsPersonneLightType[] arr = wsPersonList.getListeRetour();
        ComunityPeopleUserDto argosUserBean;
        for (final WsPersonneLightType wsPersonneLightType : arr) {
            if (wsPersonneLightType.getActif().equals("1")) {
                argosUserBean = new ComunityPeopleUserDto();
                argosUserBean.setArgosId(wsPersonneLightType.getIdArgos());
                argosUserBean.setNom(wsPersonneLightType.getNom());
                argosUserBean.setPrenom(wsPersonneLightType.getPrenom());
                argosUserBean.setEmail(wsPersonneLightType.getMail());
                argosUserBean.setLogin(wsPersonneLightType.getLogin());
                result.add(argosUserBean);
            }
        }

        // sort by lastname, firstname of the acteur
        Collections.sort(result, (o1, o2) -> {
            String s1 = "";
            String s2 = "";
            String s3 = "";
            String s4 = "";
            if (o1 != null && o2 != null) {
                if (o1.getNom() != null && o2.getNom() != null) {
                    s1 = o1.getNom().toUpperCase();
                    s2 = o2.getNom().toUpperCase();
                    s3 = o1.getPrenom().toUpperCase();
                    s4 = o2.getPrenom().toUpperCase();
                }
            }
            final int result1 = s1.compareTo(s2);
            if (result1 == 0) {
                return s3.compareTo(s4);
            } else {
                return result1;
            }
        });

        return result;
    }

    /**
     * Builds the search string.
     *
     * @param nom
     *            the nom
     * @param prenom
     *            the prenom
     * @param email
     *            the email
     * @return the string
     */
    private String buildSearchString(final String nom, final String prenom, final String email) {
        final StringBuilder searchString = new StringBuilder();
        if (nom != null && !nom.isEmpty()) {
            searchString.append("nom=" + nom);
            searchString.append(";");
        }
        if (prenom != null && !prenom.isEmpty()) {
            searchString.append("prenom=" + prenom);
            searchString.append(";");
        }
        if (email != null && !email.isEmpty()) {
            searchString.append("mail=" + email);
        }
        return searchString.toString();
    }

}