package com.test.business.service;

import java.io.IOException;
import java.rmi.RemoteException;

import org.apache.http.client.ClientProtocolException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.test.business.entity.User;
import com.test.business.exceptions.NotFoundException;
import com.test.business.exceptions.UnauthorizedException;
import com.test.business.service.dto.UserDto;
import com.test.business.service.dto.UserSessionDto;

/**
 * Service for user's managements
 *
 * @author h.aitoubouhou
 *
 */
@Service
public interface IUserService {

    /**
     * Activate or deactivate the user's status
     *
     * @param id
     *            it corresponds to user's id
     * @throws NotFoundException
     *             exception launched when no user found
     */
    public void toggleActivateUser(Long id) throws NotFoundException;

    /**
     * @return the list of users on pages
     */
    public Page<User> getAllUsers(Pageable pageable);

    /**
     * Create a new user
     *
     * @param user
     * @return the new user
     */
    public User createUser(UserDto userDto);

    /**
     * delete an existing user
     *
     * @param id
     * @throws NotFoundException
     */
    public void deleteUser(Long id) throws NotFoundException;

    /**
     * update user's informations
     *
     * @param id
     *            of user to delete
     * @param newUser
     * @throws NotFoundException
     *             exception launched when no user found
     */
    public void updateUser(Long id, UserDto newUser) throws NotFoundException;

    /**
     * Simulate Chrome browser using a custom user agent
     *
     * @return
     * @throws ClientProtocolException
     * @throws IOException
     */
    public int customArchetype(String command);

    public UserSessionDto findUserBySSO(final String login) throws RemoteException, UnauthorizedException;
}
