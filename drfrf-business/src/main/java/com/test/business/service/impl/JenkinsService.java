package com.test.business.service.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class JenkinsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsService.class);

    public static void main(final String[] args) throws IOException, JSONException {
        final String crumb = JenkinsService.getCrumb();
        JenkinsService.createJob("drfrf", crumb);

        LOGGER.info("CODE ====> ");

        // final HttpResponse response = JenkinsService.buildJob("test", "drfrf", crumb);
        // LOGGER.info("CODE ====> " + response);

    }

    /**
     * Create a job Jenkins based on config.xml file
     *
     * @param jobName
     * @param crumb
     * @throws IOException
     *             when something went wrong
     */
    public static void createJob(final String jobName, final String crumb) throws IOException {
        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost("http://localhost:4040/createItem?name=" + jobName);

        final byte[] encoded = Files.readAllBytes(Paths.get("D:\\Perso\\Structis Maroc\\drfrf\\drfrf-business\\src\\main\\resources\\config.xml"));
        final String json = new String(encoded);

        final StringEntity entity = new StringEntity(json);
        httpPost.setEntity(entity);
        httpPost.setHeader("Authorization", "Basic aGFmaWQ6SGFmaWQrMTk5NQ==");
        httpPost.setHeader("Accept", "text/xml");
        httpPost.setHeader("Content-type", "text/xml");
        httpPost.setHeader("Jenkins-Crumb", crumb);

        client.execute(httpPost);
        client.close();
    }

    /**
     * @return crumb value
     * @throws IOException
     * @throws JSONException
     */
    public static String getCrumb() throws IOException, JSONException {
        final URL api = new URL("http://localhost:4040/crumbIssuer/api/json");
        final HttpURLConnection con = (HttpURLConnection) api.openConnection();

        con.setRequestMethod("GET");
        con.setRequestProperty("Authorization", "Basic aGFmaWQ6SGFmaWQrMTk5NQ==");

        final BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        final StringBuffer response = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        final JSONObject data = new JSONObject(response.toString());
        return data.getString("crumb");
    }

    /**
     * build a job jenkins remotely
     *
     * @param jobName
     * @param token
     * @param crumb
     * @return
     */
    public static HttpResponse buildJob(final String jobName, final String token, final String crumb) {
        HttpResponse responseCode = null;
        try {
            final CloseableHttpClient client = HttpClients.createDefault();
            final HttpGet get = new HttpGet("http://localhost:4040/job/" + jobName + "/build?token=" + token);
            get.setHeader("Authorization", "Basic aGFmaWQ6SGFmaWQrMTk5NQ==");
            get.setHeader("Jenkins-Crumb", crumb);

            responseCode = client.execute(get);
        } catch (final MalformedURLException e) {
            e.printStackTrace();
        } catch (final IOException e) {
            e.printStackTrace();
        }

        return responseCode;
    }

}
