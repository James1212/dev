package com.test.business.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import com.test.business.entity.Role;

import lombok.Data;

@Data
public class UserDto implements Serializable {

    /** serialVersionUID */
    private static final long serialVersionUID = 1L;

    /** users's id */
    private Long id;

    /** user's firstName */
    private String firstName;

    /** user's lastName */
    private String lastName;

    /** user's email */
    private String email;

    // /** user's photo */
    // private Blob photo;

    /** user's password */
    private String password;

    /** the list of roles granted to the user */
    private Set<Role> roles = new HashSet<>();

}
