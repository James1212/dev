package com.test.business.service.mapper.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.test.business.entity.ProfilUser;
import com.test.business.entity.User;
import com.test.business.service.dto.UserDto;
import com.test.business.service.dto.UserSessionDto;
import com.test.business.service.mapper.IUserMapper;

import fr.xebia.extras.selma.Selma;

@Component
public class UserMapper {

    @Autowired
    private IUserMapper mapper;

    public UserMapper() {
        mapper = Selma.mapper(IUserMapper.class);
    }

    public User asUser(final UserDto userDto) {
        return mapper.asUser(userDto);
    }

    public UserDto asUserDto(final User user) {
        return mapper.asUserDto(user);
    }

    public UserSessionDto toSessionDTO(final User user, final ProfilUser profilUser) {
        return mapper.asSessionDto(user, profilUser);
    }

}
