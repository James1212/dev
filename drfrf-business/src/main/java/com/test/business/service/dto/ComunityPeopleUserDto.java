package com.test.business.service.dto;

import java.io.Serializable;

public class ComunityPeopleUserDto implements Serializable {

    private static final long serialVersionUID = -1630364529707654647L;

    private String nom;

    private String prenom;

    private String email;

    private String login;

    private String argosId;

    public String getNom() {
        return nom;
    }

    public void setNom(final String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(final String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getArgosId() {
        return argosId;
    }

    public void setArgosId(final String argosId) {
        this.argosId = argosId;
    }

    public String getIdentity() {
        String identity = "";
        if (nom != null && prenom != null) {
            identity = prenom + " " + nom.toUpperCase();
        } else {
            identity = login;
        }
        return identity;
    }
}
