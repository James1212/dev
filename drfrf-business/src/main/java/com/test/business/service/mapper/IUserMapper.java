package com.test.business.service.mapper;

import com.test.business.entity.ProfilUser;
import com.test.business.entity.User;
import com.test.business.service.dto.UserDto;
import com.test.business.service.dto.UserSessionDto;

import fr.xebia.extras.selma.IgnoreMissing;
import fr.xebia.extras.selma.IoC;
import fr.xebia.extras.selma.Mapper;

@Mapper(withIoC = IoC.SPRING, withIgnoreMissing = IgnoreMissing.ALL)
public interface IUserMapper {

    /**
     * Convert a DTO to an entity
     *
     * @param source
     *            DTO
     * @return entity
     */
    User asUser(UserDto userDto);

    /**
     * Convert an entity to a DTO
     *
     * @param source
     *            entity
     * @return DTO
     */
    UserDto asUserDto(User user);

    UserSessionDto asSessionDto(User user, ProfilUser profilUser);

}
