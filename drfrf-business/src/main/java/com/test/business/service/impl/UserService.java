package com.test.business.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.test.business.entity.ProfilUser;
import com.test.business.entity.User;
import com.test.business.exceptions.NotFoundException;
import com.test.business.exceptions.UnauthorizedException;
import com.test.business.repo.ProfilUserRepository;
import com.test.business.repo.UserRepository;
import com.test.business.security.JwtGeneratorService;
import com.test.business.service.IUserService;
import com.test.business.service.dto.UserDto;
import com.test.business.service.dto.UserSessionDto;
import com.test.business.service.mapper.impl.UserMapper;
import com.test.business.webservices.comunitypeople.CommunicationComPeople;

/**
 * Implementation of service for user's managements
 *
 * @author h.aitoubouhou
 *
 */

@Service
public class UserService implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    ProfilUserRepository profilUtilisateurRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private PasswordEncoder encoder;

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private JwtGeneratorService generator;

    /**
     * Activate or deactivate the user's status
     *
     * @param id
     *            it corresponds to user's id
     * @throws NotFoundException
     *             exception launched when no user found
     *
     * @see com.test.business.service.IUserService#toggleActivateUser(java.lang.Long)
     */
    @Override
    public void toggleActivateUser(final Long id) throws NotFoundException {
        final User user = userRepository.findOne(id);
        if (user == null) {
            throw new NotFoundException("user not found");
        }

        if (user.isActive()) {
            user.setActive(false);
        } else {
            user.setActive(true);
        }
    }

    /**
     * @return the list of users on pages
     */
    @Override
    public Page<User> getAllUsers(final Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    /**
     * Create a new user
     *
     * @param user
     * @return the new user
     */
    @Transactional
    @Override
    public User createUser(final UserDto userDto) {
        userDto.setPassword(encoder.encode(userDto.getPassword()));
        return userRepository.save(userMapper.asUser(userDto));
    }

    /**
     * delete an existing user
     *
     * @param id
     */
    @Override
    public void deleteUser(final Long id) throws NotFoundException {
        final User user = userRepository.findOne(id);
        if (user == null) {
            throw new NotFoundException("Inexisting user");
        }
        userRepository.delete(user);
    }

    /**
     * update user's informations
     *
     * @param id
     *            of user to delete
     * @param newUser
     * @throws NotFoundException
     *             exception launched when no user found
     */
    @Override
    public void updateUser(final Long id, final UserDto newUserDto) throws NotFoundException {
        final User user = userRepository.findOne(id);

        if (user == null) {
            throw new NotFoundException();
        }
        newUserDto.setId(id);
        final User newUser = userMapper.asUser(newUserDto);
        userRepository.save(newUser);
    }

    /*
     * @see com.test.business.service.IUserService#findUserBySSO(java.lang.String)
     */
    @Override
    public UserSessionDto findUserBySSO(final String login) throws UnauthorizedException, RemoteException {
        LOGGER.info(String.format("SSO authentication tentative : %s", login));
        final User user = userRepository.findUserByLogin(login);

        if (user == null) {
            LOGGER.info(String.format("SSO USER_NOT_FOUND : %s", login));
        }

        final UserSessionDto result = buildUtilisateurSession(user);
        // Check that the account is still valid
        if (result != null) {
            CommunicationComPeople comPeopleWS = null;
            try {
                comPeopleWS = new CommunicationComPeople();
            } catch (MalformedURLException | ServiceException e) {
                e.printStackTrace();
            }
            LOGGER.info(String.format("Communicate with Com.People : %s", user.getIdArgos()));
            if (!comPeopleWS.checkAccountIsValid(user.getIdArgos())) {
                throw new UnauthorizedException();
            }
        }
        LOGGER.info(String.format("SSO authentication succeeded : %s", login));
        return result;
    }

    private UserSessionDto buildUtilisateurSession(final User user) {

        // Get the profil of the user
        final ProfilUser profilUtilisateur = profilUtilisateurRepository.findByUser(user.getId());

        // Map entity to DTO
        final UserSessionDto result = userMapper.toSessionDTO(user, profilUtilisateur);
        return result;
    }

    public String generateToken(final UserSessionDto loggedUser) {
        return generator.generate(loggedUser);
    }

    /*
     * @see com.test.business.service.IUserService#customUserAgent(java.lang.String)
     */
    @Override
    public int customArchetype(final String command) {
        int exitVal = -1;
        try {
            final Runtime rt = Runtime.getRuntime();
            final String cmdString = "cmd /c " + command;

            LOGGER.info("command => " + cmdString);
            final Process pr = rt.exec(cmdString, null, new File("D://"));
            final BufferedReader input = new BufferedReader(new InputStreamReader(pr.getInputStream()));
            String line = null;

            while ((line = input.readLine()) != null) {
                System.out.println(line);
            }

            exitVal = pr.waitFor();
            LOGGER.info("Exited with error code " + exitVal);

        } catch (final Exception e) {
            System.out.println(e.toString());
            e.printStackTrace();
        }

        return exitVal;
    }

}
