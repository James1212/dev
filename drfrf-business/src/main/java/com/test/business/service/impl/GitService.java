package com.test.business.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.Proxy.Type;
import java.net.ProxySelector;
import java.net.SocketAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoFilepatternException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.lib.StoredConfig;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.transport.RefSpec;
import org.eclipse.jgit.transport.RemoteConfig;
import org.eclipse.jgit.transport.URIish;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.test.business.model.Repositories;

@Service
public class GitService {

    private static final Logger LOGGER = LoggerFactory.getLogger(GitService.class);

    public static void main(final String[] args) throws IOException, GitAPIException, URISyntaxException {
        // createLocalRepo("https://github.com/Hafidiii/dev.git", "hafidiii", "Hafid+1995");
        // createLocalRepo("https://gitlab.com/James1212/dev.git", "james1212", "Hafid+1995");
    }

    /**
     * Create and push a local repository
     *
     * @param repoUrl
     *            remote url
     * @param username
     * @param password
     * @throws URISyntaxException
     * @throws IOException
     */
    public static void pushSourceCode(final Repositories repository) throws URISyntaxException, IOException {

        final String remote = "origin";
        final String branch = "refs/heads/master";

        // Define the working directory
        // TODO create a temporary folder then remove it
        // FIXME
        final File workingDirectory = new File("D:\\" + repository.getDirectoryName());
        workingDirectory.delete();
        workingDirectory.mkdirs();

        // Create a local repository
        final Repository repo = FileRepositoryBuilder.create(new File(workingDirectory, ".git"));
        repo.create();

        try (Git git = new Git(repo)) {

            final StoredConfig config = git.getRepository().getConfig();
            config.setString("remote", "origin", "url", repository.getUrl());

            // Add remote
            final RemoteConfig remoteConfig = new RemoteConfig(config, remote);
            final URIish uri = new URIish(git.getRepository().getDirectory().toURI().toURL());
            remoteConfig.addURI(uri);
            remoteConfig.addFetchRefSpec(new RefSpec("+refs/heads/*:refs/remotes/" + remote + "/*"));
            remoteConfig.update(config);
            config.save();

            git.add().addFilepattern(".").call();

            // Commit
            git.commit().setMessage("My first commit").call();
            git.tag().setName("tag").call();

            LOGGER.info(repo.getDirectory().getAbsolutePath());

            // Proxy setting
            ProxySelector.setDefault(new ProxySelector() {
                final ProxySelector delegate = ProxySelector.getDefault();

                @Override
                public List<Proxy> select(final URI uri) {
                    // Filter the URIs to be proxied
                    if (uri.toString().contains("github") && uri.toString().contains("https")
                            || uri.toString().contains("gitlab") && uri.toString().contains("https")) {
                        return Arrays.asList(new Proxy(Type.HTTP, InetSocketAddress.createUnresolved("localhost", 3128)));
                    }
                    if (uri.toString().contains("github") && uri.toString().contains("http")
                            || uri.toString().contains("gitlab") && uri.toString().contains("http")) {
                        return Arrays.asList(new Proxy(Type.HTTP, InetSocketAddress.createUnresolved("localhost", 3128)));
                    }
                    // revert to the default behaviour
                    return delegate == null ? Arrays.asList(Proxy.NO_PROXY) : delegate.select(uri);
                }

                @Override
                public void connectFailed(final URI uri, final SocketAddress sa, final IOException ioe) {
                    if (uri == null || sa == null || ioe == null) {
                        throw new IllegalArgumentException("Arguments can't be null.");
                    }
                }
            });

            // pull command
            git.pull().setRemote(remote).setCredentialsProvider(new UsernamePasswordCredentialsProvider(repository.getUsername(), repository.getPassword()))
                    .call();

            // push command
            final RefSpec spec1 = new RefSpec(branch + ":" + branch);
            git.push().setRemote(remote).setRefSpecs(spec1)
                    .setCredentialsProvider(new UsernamePasswordCredentialsProvider(repository.getUsername(), repository.getPassword())).call();

            LOGGER.info("Done !");

        } catch (final IOException e) {
            e.printStackTrace();
        } catch (final NoFilepatternException e) {
            e.printStackTrace();
        } catch (final GitAPIException e) {
            e.printStackTrace();
        }

    }

}
