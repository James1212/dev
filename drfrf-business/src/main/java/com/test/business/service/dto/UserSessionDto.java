package com.test.business.service.dto;

public class UserSessionDto {

    private Long id;
    private String login;
    private String firstName;
    private String lastName;
    private String codeProfil;
    private String token;

    public UserSessionDto() {
    }

    public UserSessionDto(final Long idUtilisateur, final String login, final String codeProfil) {
        id = idUtilisateur;
        this.login = login;
        this.codeProfil = codeProfil;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long idUtilisateur) {
        id = idUtilisateur;
    }

    public String getNom() {
        return firstName;
    }

    public void setNom(final String nom) {
        firstName = nom;
    }

    public String getPrenom() {
        return lastName;
    }

    public void setPrenom(final String prenom) {
        lastName = prenom;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getCodeProfil() {
        return codeProfil;
    }

    public void setCodeProfil(final String codeProfil) {
        this.codeProfil = codeProfil;
    }

    public String getToken() {
        return token;
    }

    public void setToken(final String token) {
        this.token = token;
    }

}
